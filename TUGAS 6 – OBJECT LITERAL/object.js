//Soal No. 1 (Array to Object)
var now = new Date();
var thisYear = now.getFullYear();

function arrayToObject(arr)
{
  for (var i = 0; i < arr.length; i++) 
  {
      var usia = 0;
      var no = i + 1;
      if (arr[i][3] == null || arr[i][3] > thisYear) 
      {
        usia = "Invalid birth year";
      } 
      else
      {
        usia = thisYear - arr[i][3];
      }

      var arrObject = {
        firstName: arr[i][0],
        lastName: arr[i][1],
        gender: arr[i][2],
        age: usia,
      };
      console.log(
        no + ". " + arrObject.firstName + " " + arrObject.lastName + ": "
      );
      console.log(arrObject);
  } 
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people);
arrayToObject(people2);
arrayToObject([]);
console.log('----------------------------------------------');
//Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) 
{
  var shopping = {}
  var barang = [
                  ['Sepatu Stacattu', 1500000],
                  ['Baju Zoro', 500000],
                  ['Baju H&N', 250000],
                  ['Sweater Uniklooh', 175000],
                  ['Casing Handphone', 50000]
               ]
  
  if(memberId === undefined && money === undefined)
  {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  }
  
  if(memberId === '')
  {
    return "Mohon maaf, toko X hanya berlaku untuk member saja"
  } 
  else {
    shopping.memberId = memberId
  }
  
  if(money <= 50000 )
  {
    return 'Mohon maaf, uang tidak cukup'
  } 
  else 
  {
    shopping.money = money
  }
  
  var jumlahhargabarang = 0
  shopping.listPurchased = []
  for(i = 0; i < barang.length; i++)
  {
    
    if(money > barang[i][1])
    {
      shopping.listPurchased.push(barang[i][0])
      jumlahhargabarang += barang[i][1]
    }
    shopping.changeMoney = money - jumlahhargabarang
  }
   return shopping
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
console.log('----------------------------------------------');
//Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang)
{
  var arr = [];
  for (var i = 0; i < arrPenumpang.length; i++) 
  {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    for(j=0;j<=(rute.length)+1;j++)
    {
      if(rute[j]==arrPenumpang[i][1])
      {
        var naik = j;
      }   
    }
    for(k=0;k<=(rute.length)+1;k++)
    {
      if(rute[k]==arrPenumpang[i][2])
      {
        var turun = k;
      }   
    }
    if(naik < turun)
    {
      var biayapp = (turun - naik)*2000;
      arr[i] = {
          penumpang: arrPenumpang[i][0],
          naikdari: arrPenumpang[i][1],
          turun: arrPenumpang[i][2],
          bayar: biayapp,
      };
    }
    else if (naik > turun)
    {
      var biayapp = (naik - turun)*2000;
      arr[i] = {
          penumpang: arrPenumpang[i][0],
          naikdari: arrPenumpang[i][1],
          turun: arrPenumpang[i][2],
          bayar: biayapp,
      };
    }  
  }
  return arr;
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));