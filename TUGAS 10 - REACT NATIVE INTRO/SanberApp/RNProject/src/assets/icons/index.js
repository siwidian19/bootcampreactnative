import IconHome from './home.svg'
import IconHomeActive from './homeActive.svg'
import IconAkun from './akun.svg'
import IconAkunActive from './akunActive.svg'
import IconPesanan from './pesanan.svg'
import IconPesananActive from './pesananActive.svg'

export { IconHome, IconHomeActive, IconAkun, IconAkunActive, IconPesanan, IconPesananActive }