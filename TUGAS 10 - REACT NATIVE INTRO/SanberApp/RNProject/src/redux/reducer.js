import { combineReducers } from "redux"

const initialState = {
  name: 'xxx'
}

const initialStateSignup= {
  form:{
    userName:'',
    email:'',
    password:''
  },
  title: 'Signup',
  desc: 'Ini adalah desc untuk SignUp',
}

const SignupReducer = (state = initialStateSignup, action) => {
  if(action.type === 'SET_FORM'){
    return{
      ...state,
      form:{
        ...state.form,
        [action.inputType] : action.inputValue
      }
    }
  }
  return state;
}

const initialStateLogin= {
  form:{
    userName:'Dian',
    password:'12'
  },
  info: 'Tolong masukan password anda dengan benar',
  isLogin: true,

}


const LoginReducer = (state = initialStateLogin, action) => {
  if(action.type === 'SET_FORM'){
    return{
      ...state,
      form:{
        ...state.form,
        [action.inputType] : action.inputValue
      }
    }
  }

  return state;
}

const reducer = combineReducers({ SignupReducer, LoginReducer });

export default reducer;