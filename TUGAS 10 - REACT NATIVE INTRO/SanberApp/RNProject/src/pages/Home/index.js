import React from 'react'
import { Dimensions, ImageBackground, StyleSheet, Text, View, Image } from 'react-native'
import { ImageHeader } from '../../assets'

const Home = (route) => {
  const { userName } = route.params;
  return (
    <View style={styles.page}>
      <ImageBackground source={ImageHeader} style={styles.header}>
        <View style={styles.greeting}>
          <Text style={{fontSize:18, fontFamily:'Raleway-Reguler'}}>Selamat Datang, </Text>
          <Text style={{fontSize:14, fontFamily:'Raleway-ExtraBold'}}>{(userName)}</Text>
        </View>
      </ImageBackground>
    </View>
  )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.25,
    paddingHorizontal: 30,
    paddingVertical: 10
  },
  greeting: {
    marginTop: windowWidth * 0.25,

  }
})
