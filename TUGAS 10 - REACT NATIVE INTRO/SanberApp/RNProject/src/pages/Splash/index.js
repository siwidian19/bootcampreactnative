import React, { useEffect } from 'react'
import { ImageBackground, StyleSheet, Text, View, Image } from 'react-native'
import { Logo, SplashBackground } from '../../assets'

const Splash = ({ navigation }) => {

  useEffect(() => {
    setTimeout( ()=> {
      navigation.replace('Welcome');
    }, 3000)
  }, [navigation]);

  return (
    <ImageBackground source={SplashBackground} style={styles.background}>
      <Image source={Logo} style={styles.logo}></Image>
    </ImageBackground>
  )
}

export default Splash

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    //alignSelf: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  }
})
