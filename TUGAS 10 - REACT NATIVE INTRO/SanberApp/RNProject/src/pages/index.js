import Home from './Home'
import Akun from './Akun'
import Pesanan from './Pesanan'
import Splash from './Splash'
import Login from './Login'
import Signup from './Signup'
import Welcome from './Welcome';

export { Home, Akun, Pesanan, Splash, Login, Signup, Welcome }

