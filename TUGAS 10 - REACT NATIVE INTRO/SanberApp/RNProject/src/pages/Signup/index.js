import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, ImageBackground, Image, ScrollView, TouchableOpacity } from 'react-native'
import { Button, Input } from '../../components'
import { Logo, SplashBackground } from '../../assets'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { useSelector, useDispatch } from 'react-redux';
import { setForm } from '../../redux';

const Signup = ({navigation}) => {
  const SignupReducer = useSelector(state => state.SignupReducer)
  const dispatch = useDispatch();
  // const [form, setForm] = useState({
  //   userName:'',
  //   email:'',
  //   password:''
  // })

  const sendData = () => {
    console.log('data yang dikirim', SignupReducer.form)
    // axios.post('url',form);
  }

  const onInputChange = (value, inputType) => {
    // setForm({
    //   ...form,
    //   [input]:value,

    // })
    dispatch(setForm(inputType, value));
  }

  return (
    <ImageBackground source={SplashBackground} style={styles.background}>
      <View style={{ padding: 20 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ width: 35, height: 35 }}>
            <Button type='icon' name='back' onPress={() => navigation.goBack()} />
          </View>
          <View style={{ width: 150, height: 150, marginTop: 8 }}><Image source={Logo} style={styles.logo}></Image></View>
          <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 16, maxWidth: 200 }}>Silakan mengisi beberapa data untuk proses daftar anda</Text>
          <View style={{ height: 65 }} />
          <Input placeholder='Nama Lengkap' value={SignupReducer.form.userName} onChangeText={value => onInputChange(value, 'userName')} />
          <View style={{ height: 35 }} />
          <Input placeholder='Email' value={SignupReducer.form.email} onChangeText={value => onInputChange(value, 'email')} />
          <View style={{ height: 35 }} />
          <Input placeholder='Password' value={SignupReducer.form.password} onChangeText={value => onInputChange(value, 'password')} secureTextEntry={true} />
          <View style={{ height: 80 }} />
          <Button title='Daftar' onPress={sendData}></Button>
        </ScrollView>
      </View>
    </ImageBackground>
  )
}

export default Signup

const styles = StyleSheet.create({


  background: {
    flex: 1,

  },

  logo: {
    width: 150,
    height: 150,
    resizeMode: 'contain'
  },
})
