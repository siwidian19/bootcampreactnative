import React from 'react'
import { StyleSheet, Text, View, ImageBackground, Image, Alert } from 'react-native'
import { Logo, SplashBackground } from '../../assets'
import ActionButton from './ActionButton'

const Welcome = ({ navigation }) => {
  const handleGoTo = (screen) => {
    navigation.navigate(screen)
  }
  return (
    <ImageBackground source={SplashBackground} style={styles.background}>
      <View style={styles.container}>
        <View><Image source={Logo} style={styles.logo}></Image></View>
        <Text style={{ fontSize: 18, fontFamily: 'Raleway-ExtraBold', marginBottom: 76 }}>Selamat Datang di O-BOOKSTORE</Text>
        <ActionButton
          desc="Silakan 'Login', jika anda sudah memiliki akun"
          title="Masuk"
          onPress={() => handleGoTo('Login')} />
        <ActionButton
          desc="atau silakan 'Daftar' jika anda belum memiliki akun"
          title="Daftar"
          onPress={() => handleGoTo('Signup')} />
      </View>
    </ImageBackground>
  )
}

export default Welcome

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },

  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  logo: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
    marginBottom: 10
  },
})
