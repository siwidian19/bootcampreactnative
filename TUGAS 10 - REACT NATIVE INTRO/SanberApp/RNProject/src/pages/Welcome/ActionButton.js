import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from '../../components'

const ActionButton = ({ desc, title, onPress }) =>{
  
  return(
    <View style={{marginBottom:40, maxWidth:225}}>
      <Text style={{fontSize:12, color:'#606060', textAlign: 'center', paddingHorizontal:'15%',marginBottom:6}}>{desc}</Text>
      <Button title={title} onPress={onPress}/> 
    </View>
    
  )
}

export default ActionButton

const styles = StyleSheet.create({})
