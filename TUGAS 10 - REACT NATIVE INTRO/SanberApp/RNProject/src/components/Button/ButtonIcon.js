import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const ButtonIcon = ({...rest}) => {
  return (
    <TouchableOpacity {...rest}>
      {rest.name ==='back' && <Icon name='reply' size={30}></Icon>}
    </TouchableOpacity>
  )
}

export default ButtonIcon;


