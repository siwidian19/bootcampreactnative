import React from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'


const Input = ({placeholder,...rest}) => {
  return (
    <TextInput 
      style={styles.input} 
      placeholder={placeholder}
      {...rest}
    />
  )
}

export default Input

const styles = StyleSheet.create({
  input:{
    borderRadius:25,
    paddingVertical:12,
    paddingHorizontal:18,
    fontSize:14,
    backgroundColor:'white'



  }
})
