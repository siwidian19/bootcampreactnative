import firebase from 'firebase'

firebase.initializeApp({
  apiKey: "AIzaSyBBqTSPZMGJwytpTvQWyGE6bbUCd3H93rY",
  authDomain: "rnproject-99d87.firebaseapp.com",
  projectId: "rnproject-99d87",
  storageBucket: "rnproject-99d87.appspot.com",
  messagingSenderId: "743929813458",
  appId: "1:743929813458:web:9327c0b977fd95aa9a100b"
})

const FIREBASE = firebase;

export default FIREBASE;