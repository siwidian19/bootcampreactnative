import React from "react";
import { View, Text, StyleSheet, Button, Alert } from "react-native";


//import { AuthContext } from "./context";
import { About } from "./AboutScreen";
import { Add } from "./AddScreen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});


const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Skill = ({ navigation }) => (
  <ScreenContainer>
    <Text>Skill Screen</Text>

  </ScreenContainer>
);

