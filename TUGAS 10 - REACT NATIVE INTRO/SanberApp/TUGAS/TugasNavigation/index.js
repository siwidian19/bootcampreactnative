import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { Login } from "./LoginScreen";
import { Skill } from "./SkillScreen";
import { About } from "./AboutScreen";
import { Add } from "./AddScreen";
import { Project } from "./ProjectScreen";


const LoginStack = createStackNavigator();
const AboutStack = createStackNavigator();
const SkillStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const DrawerScreen = ()=>(
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={Skill}/>
    <Drawer.Screen name="About" component={About}/>
  </Drawer.Navigator>
);

const TabsScreen = ()=>(
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={DrawerScreen} />
    <Tabs.Screen name="Project" component={Project} />
    <Tabs.Screen name="Add" component={Add} />
  </Tabs.Navigator>
);

export default() => (
  <NavigationContainer>
    <LoginStack.Navigator>
      <LoginStack.Screen name="Login" component={Login} />
      <LoginStack.Screen 
      name="Skill" 
      component={TabsScreen} 
      options={ ({route}) => ({title: route.params.name}) }
    />
    </LoginStack.Navigator> 
  </NavigationContainer>
);