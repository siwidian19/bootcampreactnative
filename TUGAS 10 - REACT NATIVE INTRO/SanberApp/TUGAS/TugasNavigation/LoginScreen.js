import React from "react";
import { View, Text, StyleSheet, Button, Alert } from "react-native";

//import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Login = ({ navigation }) => (
  <ScreenContainer>
    <Text>Login Screen</Text>
    <Button
      title="Menuju Halaman Skill" 
      onPress={() => {
        navigation.navigate("Skill",{
        screen: "Login",
        params: {name: "Menuju Halaman Skill"}
      });
    }}
    />
  </ScreenContainer>
);

