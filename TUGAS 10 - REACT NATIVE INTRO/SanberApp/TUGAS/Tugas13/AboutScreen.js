import React, { Component } from 'react';
import {Alert, StyleSheet, Text, Image, View, TextInput,Button} from 'react-native';
import Icon1 from 'react-native-vector-icons/FontAwesome'; 
import Icon2 from 'react-native-vector-icons/Ionicons';
//import FontAwesome, { Icons } from 'react-native-vector-icons/FontAwesome';
//import Icon from 'react-native-vector-icons/MaterialIcons';

export default class About extends Component{


    render(){
        return(
        <View style={styles.container}>
          <Image source={require('./assets/background.png')} resizeMode = 'cover' style={styles.backgroundImage}/>
          <Text style={styles.tema}>ABOUT ME</Text>   
          <Image style={styles.image} source={require('./images/aboutme.png')}/> 
          <Text style={styles.name}>Siwi Dian Priyanti</Text>
          <Text style={styles.tittle}>REACT NATIVE DEVELOPER</Text>   
          <View style={styles.square1}> 
            <Text style={styles.portoFolio}>Portofolio</Text>
            <TextInput style={{marginTop:-20, marginBottom:10}} underlineColorAndroid='#003366' />
            <View style={styles.sectionStyleP}>
              <Icon1 style={styles.logoBrand} name="gitlab" size={40} />
              <Icon1 style={styles.logoBrand} name="github" size={40} />
            </View>
            <View style={styles.sectionStyleP}>
              <Text>@siwidian</Text>
              <Text>@siwidian</Text>
            </View>
          </View>
          <View style={styles.square2}> 
            <Text style={styles.portoFolio}>Hubungi Saya</Text>
            <TextInput style={{marginTop:-20, marginBottom:10}} underlineColorAndroid='#003366' />
            <View style={styles.sectionStyleH}>
              <View style={styles.sectionStyle}>
                <Icon2 style={styles.logoBrand} name="logo-facebook" size={40} color="#3b5998" />
                <Text style={{padding:10}}>@siwidian</Text>  
              </View>
              <View style={styles.sectionStyle}>
                <Icon2 style={styles.logoBrand} name="logo-twitter" size={40} color="#1da1f2" />
                
                <Text style={{padding:10}}>@siwidian</Text>
              </View>
              <View style={styles.sectionStyle}>
                <Icon1 style={styles.logoBrand} name="instagram" size={40} color="#c13584" /> 
                <Text style={{padding:10}}>@siwidian</Text>
              </View>
            </View>
          </View>
        </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor:'#2196F3',
        flexDirection: 'column',
        //paddingBottom: 70,
        //fontFamily: 'Roboto-Medium',
        height: '100%',
        display: 'flex',
    },
    logoBrand:{
        padding: 5
    },
    square1: {
        width: '90%',
        height: 130,
        backgroundColor: 'white',
        margin:10
    },
    square2: {
        width: '90%',
        height: 210,
        backgroundColor: 'white',
        margin:10
    },
    portoFolio:{
        padding: 5,
        marginLeft: 5,
        fontSize: 15,
        fontWeight: 'bold',
        color: '#003366'
    },
    sectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    sectionStyleP: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flex: 1,
            
    },
    sectionStyleH: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        
    },
    image:{
        width: 200,
        height: 200,
        resizeMode: 'contain',
        //alignSelf: 'flex-start',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        borderRadius: 200/2,
    },
    name:{
        paddingTop: 20,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#003366'
    },
    tittle:{
        
        fontSize: 15,
        fontWeight: 'bold',
        color: '#3EC6FF',
        paddingBottom: 30
    },
    backgroundImage:{
        //flex: 1,
        position: 'absolute',
        justifyContent: 'center',
        width: null,
        height: null,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        opacity: 0.6,
    },
    tema:{
        justifyContent: 'flex-start',
        fontSize: 30,
        color: '#141823',
        fontWeight: 'bold',
        marginBottom:20
    },
       
});