import React, { Component } from 'react';
import {Alert, StyleSheet, Text, Image, View, TextInput,Button} from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome'; 
import Icon from 'react-native-vector-icons/Ionicons';
//import FontAwesome, { Icons } from 'react-native-vector-icons/FontAwesome';
//import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends Component{

	constructor(props){
        super(props)
        this.state={
            username:'',
            password:''
        }
    }

	render(){
		return(
			<View style={styles.container}>
				<Image source={require('./assets/background.png')} resizeMode = 'cover' style={styles.backgroundImage}/>
				<Image style={styles.image} source={require('./assets/tema.png')}/>
				<Text style={styles.tema}>HI...EXPERT!</Text>
        <View style={styles.sectionStyle}>
        	
        	<TextInput style={styles.username} placeholder="Type Your Username" /> 
        </View>
        <View style={styles.sectionStyle}>
        	
        	<TextInput style={styles.password} placeholder="Type Your Password" secureTextEntry={true}/>
        </View>
        <Text style={styles.forget}>Forget your password?</Text>
        <View style={styles.loginButton}>
            <Button title="Login" onPress={this._onPressLogin} color="#000080" />
        </View>
        <Text style={styles.signup}>Don't have account? Sign Up or Using</Text>
        <View style={styles.sectionStyle}>
	        <Icon style={styles.logoBrand} name="logo-facebook" size={25} color="#3b5998" />
	        <Icon style={styles.logoBrand} name="logo-twitter" size={25} color="#1da1f2" />
	        <Icon style={styles.logoBrand} name="logo-google" size={25} color="#DB4437" />
        </View>
    </View>

		)
	}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor:'#2196F3',
        flexDirection: 'column',
    		//paddingBottom: 70,
    		//fontFamily: 'SansSerif',
    		height: '100%',
  			display: 'flex',
    },
    sectionStyle: {
		    flexDirection: 'row',
		    justifyContent: 'center',
		    alignItems: 'center',
		    
  	},
  	logoBrand:{
  			padding: 10
  	},
    image:{
		    width: 120,
		    height: 120,
		    resizeMode: 'contain',
		    //alignSelf: 'flex-start',
		    flexDirection: 'column',
		    justifyContent: 'flex-start',
    },
    backgroundImage:{
    		//flex: 1,
    		position: 'absolute',
    		justifyContent: 'center',
    		width: null,
    		height: null,
    		top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        opacity: 0.6,
    },
    tema:{
        justifyContent: 'flex-start',
  			fontSize: 30,
  			color: '#141823',
  			fontWeight: 'bold',
  			marginBottom:20
    },
    forget:{
    	marginLeft: 'auto',
    	flexDirection: 'row',
    	width: '90%',
    	textAlign: 'right',
    	marginRight:20
    },
    signup:{
    	paddingTop:70,
    	fontWeight: 'bold',
    },
    signup2:{
    	paddingTop:50,
    },
    username:{
        height: 40,
		    borderColor: '#ffffff',
		    backgroundColor: 'white',
		    borderWidth: 1,
		    color:'#ffffff',
		    width: '90%',
        padding: 10,
        marginBottom: 10,
		    },
    password: {
        height: 40,
		    borderColor: '#ffffff',
		    backgroundColor: 'white',
		    borderWidth: 1,
		    color:'#ffffff',
		    width: '90%',
        padding: 10,
        marginBottom: 5,
    },
    loginButton:{
        width:'90%',
        marginBottom: 10,
        marginTop: 30,
    }
});