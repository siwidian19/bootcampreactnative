import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
//import YoutubeUi from './TUGAS/Tugas12/App';
//import Component from './Latihan/Component/Component';
//import Component from './Latihan/Latihan2/Index';
//import Login from './TUGAS/Tugas13/LoginScreen';
//import About from './TUGAS/Tugas13/AboutScreen';
//import TodoApp from './TUGAS/Tugas14/App';
//import Navigation from './TUGAS/Tugas15/index';
//import Navigation from './TUGAS/TugasNavigation/index';
//import Quiz from './TUGAS/Quiz3/index';
//import RestApi from './Latihan/LatihanRestApi/index';
//import AppContainer from './Latihan/LatihanStateRedux/routes/index';
//import StateProps from './Latihan/LatihanStateProps/StateProps';
//import Props from './Latihan/LatihanStateProps/Props';
//import Modals from './Latihan/LatihanModals/Modals';
//import Navigasi from './Latihan/LatihanNavigasi/ReactNavigasiParams';
import TabViewExample from './Latihan/LatihanTabView/TabView';
import LocalImage from './Latihan/LatihanImage/LocalImage';
import Alert from './Latihan/LatihanAlert/Alert';
import Listener from './Latihan/LatihanListener/ClickListener';
import FontStyle from './Latihan/LatihanFont/FontStyle';
import TextArea from './Latihan/LatihanTextArea/TextArea';
import Lotie from './Latihan/LatihanLotie/Lotie';
import GetDetailApi from './Latihan/LatihanGetDetailApi/GetDetailApi';
import StopWatch from './Latihan/LatihanMoment/StopWatch';
import AsyncStorage from './Latihan/LatihanAsyncStorage/AsyncStorage'
import Toast from './Latihan/LatihanToast/Toast';
import Platform from './Latihan/LatihanPlatform/Platform';
import Responsive from './Latihan/LatihanResponsive/Responsive';
//import UploadImage from './Latihan/LatihanUploadImage/UploadImage';
import HitungJarak from './Latihan/LatihanGeolib/HitungJarak';
import Swipe from './Latihan/LatihanSwipe/Swipe';
import Aspect from './Latihan/LatihanAspectRatio/AspectRatio';
import Intro from './Latihan/LatihanIntro/rnIntro';
import Testing from './Latihan/LatihanTesting/index';
import UbahTanggal from './Latihan/LatihanChangeDate/ChangeDate';
import Realm from './Latihan/LatihanRealm/Realm';

export default function App() {
  return (
    
    <Realm/>
    //<UbahTanggal/>
    //<Testing/>
    //<Intro/>
    //<YoutubeUi/>
    //<Component/>
    //<Login/>
    //<About/>
    //<TodoApp/>
    //<Navigation/>
    //<Quiz/>
    //<RestApi/>
    //<AppContainer/>
    //<StateProps/>
    //<Props/>
    //<Modals/>
    //<Navigasi/>
    //<TabViewExample/>
    //<LocalImage/>
    //<Alert/>
    //<Listener/>
    //<FontStyle/>
    //<TextArea/>
    //<Lotie/>
    //<GetDetailApi/>
    //<StopWatch/>
    //<AsyncStorage/>
    //<Toast/>
    //<Platform/>
    //<Responsive/>
    //<UploadImage/>
    //<HitungJarak/>
    //<Swipe/>
    //<Aspect/>
    //<View style={styles.container}>
      //<Text>Open up App.js to start working on your App!</Text>
    //</View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
