//TUGAS NO.1
function range(awal,akhir) 
{
  var arr=[];
  if(awal != '' && akhir != '')
  {
    if(awal<akhir)
    {
      for(i=awal;i<=akhir; i++)
      {
        var data=0;
        data += i;
        arr.push(data);
      }
    }
    else if (awal>akhir)
    {
      for(i=awal;i>=akhir; i--)
      {
        var data=0;
        data += i;
        arr.push(data);
  
      }
    }
    else if (awal == akhir && awal != '' || akhir != '')
    {
      return [-1];
    }
  }
  else if(awal == undefined && akhir == undefined)
  {
    return [-1];
  }
 
  return arr; 
}

console.log (range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log (range(54,50));
console.log(range());
console.log("--------------------------------------");
//TUGAS NO.2
function rangeWithStep(startNum, finishNum, step) 
{
  var arr2 = [];
  if(startNum<finishNum)
  {
    if(step == 2)
    {
      for(i=startNum; i<= finishNum; i++)
      {
        if (i%step==1)
        {
          arr2.push(i);
        }
      }
    }
    else if (step == 3)
    {
      for(i=startNum; i<= finishNum; i+=3)
      {
        data = data + i;
        arr2.push(i);
           
      }
    }
  }
  else if(startNum>finishNum)
  {
    if(step == 1)
    {
      for(i=startNum;i>=finishNum; i-=step)
      {
        var data=0;
        data += i;
        arr2.push(data);
  
      }
    }
    else if (step == 4)
    {
      for(i=startNum;i>=finishNum; i-=step)
      {
        var data=0;
        data += i;
        arr2.push(data);
  
      }
    }
    
  }
  return arr2; 

}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log("--------------------------------------");
//TUGAS NO.3
function sum(startNum, finishNum, step) 
{
  var arr3 = [];
  if(startNum != '' && finishNum != '')
  {
    if(startNum<finishNum)
    {
      if(step==2)
      {
        var data = 0;
        for(i=startNum; i<= finishNum; i+=step)
        {
          data += i;
        }
        return [data];
      }
      else if(step == undefined)
      {
        stepfix = 1;
        var data = 0;
        for(i=startNum; i<= finishNum; i+=stepfix)
        {
          data += i;
        }
        return [data];
      }
    }
    else if(startNum>finishNum)
    {
      if(step==2)
      {
        var data = 0;
        for(i=startNum; i>= finishNum; i-=step)
        {
          data += i;
        }
        return [data];
      }
      else if(step == undefined)
      {
        stepfix = 1;
        var data = 0;
        for(i=startNum; i>= finishNum; i-=stepfix)
        {
          data += i;
        }
        return [data];
      }
    }
    else if((startNum!='' || finishNum!='' || step != '')&&(startNum == finishNum || startNum == step || finishNum == step)) 
    {
      return [1];
    }
  }
  else
  {
    return [0];
  }
   
  return arr3; 

}
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("--------------------------------------");
//TUGAS NO.4
function dataHandling(input){
  var data='';
  var i;
  for (i=0; i<input.length; i++ ){
    data += 'Nomor ID: ' + input[i][0] + '\n' + 
                'Nama Lengkap: ' + input[i][1] + '\n' + 
                'TTL: ' + input[i].slice(2,4).join(' ') + '\n' + 
                'Hobi: ' + input[i][4]+ '\n' + '\n';
  }
  return data;
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

console.log(dataHandling(input));
console.log("--------------------------------------");
//TUGAS NO.5
function balikkata(str)
{
  return str.split('').reverse().join('');
}
console.log(balikkata("Kasur Rusak")) // kasuR rusaK
console.log(balikkata("SanberCode")) // edoCrebnaS
console.log(balikkata("Haji Ijah")) // hajI ijaH
console.log(balikkata("racecar")) // racecar
console.log(balikkata("I am Sanbers")) // srebnaS ma I 
console.log("--------------------------------------");
//TUGAS NO.6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
    function dataHandling2(input) {
      input.splice(1, 1, "Roman Alamsyah Elsharawy");
      input.splice(4, 1, "Pria");
      input.splice(5, 0, "SMA Internasional Metro");
      console.log(input);

      var bulan = input[3].split('/');
      var nmbulan;
      switch (bulan[1]) {
        case '01':
          nmbulan = 'Januari';
          break;
        case '02':
          nmbulan = 'Februari';
          break;
        case '03':
          nmbulan = 'Maret';
          break;
        case '04':
          nmbulan = 'April';
          break;
        case '05':
          nmbulan = 'Mei';
          break;
        case '06':
          nmbulan = 'Juni';
          break;
        case '07':
          nmbulan = 'Juli';
          break;
        case '08':
          nmbulan = 'Agustus';
          break;
        case '09':
          nmbulan = 'September';
          break;
        case '10':
          nmbulan = 'Oktober';
          break;
        case '11':
          nmbulan = 'November';
          break;
        case '12':
          nmbulan = 'Desember';
          break;
        default:
      }
      console.log(nmbulan);
      bulan.sort(function(value1, value2) { return value1 < value2});
      console.log(bulan);
      var tanggal = bulan.join("-");
      console.log(tanggal);
      input.splice(0,1, input[1].split("").slice(0, 14).join(''));
      input.pop();
      input.pop();
      input.pop();
      input.pop();
      input.pop();
      console.log(input);
    }
    dataHandling2(input);