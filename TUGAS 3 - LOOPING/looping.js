//TUGAS 1 LOOPING-WHILE
var kata = "I love coding";
var i = 20;
var x = 1;

console.log("LOOPING PERTAMA");
while(x<=20)
{   
    if(x % 2 == 0)
    {
        console.log(x,kata);       
    }
    x++;  
}

console.log("LOOPING KEDUA");
while(i>=1)
{   
    if(i % 2 == 0)
    {
        console.log(i,kata);       
    }
    i--;  
}
console.log('-------------------------------');
//TUGAS 2 Looping menggunakan for
var ganjil = "Santai";
var genap = "Berkualitas";
var lipat3ganjil = "I Love Coding" 
var i = 20

for(i=1;i<=20;i++)
{
    if(i % 2 == 0)
    {
        console.log(i,genap);       
    }
    else if (i % 2 == 1 && i % 3  == 0)
    {
        console.log(i,lipat3ganjil);
    }
    else
    {
        console.log(i,ganjil);
    }
}
console.log('-------------------------------');
//TUGAS 3 Membuat Persegi Panjang #
var baris = 4;
var kolom = 8;
var kata = "";

for(i=1;i<=baris;i++)
{
    for(y=1;y<=kolom;y++)
    {
        kata += "#";
    }
    kolom = 0;
    y=0;
    console.log(kata);
}
console.log('-------------------------------');
//TUGAS 4 Membuat Tangga
var baris = 7;
var kata = "";

for(i=1;i<=baris;i++)
{
    kata ="";
    for(y=1;y<=i;y++)
    {
        kata += "#";
    }
    console.log(kata);
}
console.log('-------------------------------');
//TUGAS 5 Membuat Papan Catur
var baris = 8;
var kolom = 4;
var kata1="";
var kata2="";
var i = 0;
var y = 0;

for(i=1;i<=baris;i++)
{   
    kata1 ="";
    if(i % 2 == 1)
    {   
        kata1 ="";
        for(y=1;y<=kolom;y++)
        {
            kata1 += " #";            
        }
        console.log(kata1);
    }
    else
    {
        kata1 ="";
        for(y=1;y<=kolom;y++)
        {
            kata1 += "# ";
        }     
        console.log(kata1);
    }
    
}