//TUGAS No. 1
var teriak = function ()
{
    var kata="Halo Sanbers!";
    return kata;
}

console.log(teriak());
console.log('-------------------------------');
//TUGAS No. 2
function kalikan(num1, num2)
{
    return num1*num2;
}

var num1=12;
var num2=4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);
console.log('-------------------------------');
//TUGAS No. 3
function introduce(name, age, address,hobby)
{
    var kalimat= "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";
    return kalimat;
} 

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
var perkenalan = '';
 
perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);