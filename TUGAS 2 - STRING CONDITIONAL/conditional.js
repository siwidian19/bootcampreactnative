//TUGAS 1
var nama = "jane"
var peran = "penyihir"

if(nama=== "" && peran === "")
{
    console.log("Nama harus diisi!");
}
else if(nama=== "john" && peran === "")
{
    console.log("Halo John, Pilih peranmu untuk memulai game!");
}
else if(nama=== "jane" && peran === "penyihir")
{
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
}
else if(nama=== "jenita" && peran === "guard")
{
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
}
else
{
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}

//TUGAS 2
var hari = 30; 
var bulan = 5; 
var tahun = 2020;

switch(bulan) 
{
  case 1:   { console.log(hari + ' Januari ' + tahun); break; }
  case 2:   { console.log(hari + ' Februari ' + tahun); break; }
  case 3:   { console.log(hari + ' Maret ' + tahun); break; }
  case 4:   { console.log(hari + ' April ' + tahun); break; }
  case 5:   { console.log(hari + ' Mei ' + tahun); break; }
  case 6:   { console.log(hari + ' Juni ' + tahun); break; }
  case 7:   { console.log(hari + ' Juli ' + tahun); break; }
  case 8:   { console.log(hari + ' Agustus ' + tahun); break; }
  case 9:   { console.log(hari + ' September ' + tahun); break; }
  case 10:   { console.log(hari + ' Oktober ' + tahun); break; }
  case 11:   { console.log(hari + ' November ' + tahun); break; }
  case 12:   { console.log(hari + ' Desember ' + tahun); break; }
  default:   { console.log('Belum input bulan tanggal'); }
  
}
