var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function bacabuku(time, book, i)
{
	if(i<books.length)
	{
		readBooks(time,books[i], function(sisa){
			if(sisa>0){
				i++;
				bacabuku(sisa, books, i);
			}
		})
	}
}

bacabuku(10000,books,0)